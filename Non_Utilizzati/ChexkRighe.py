 # Controllo se esiste I TABELLA - II RIGA       
            try:
                                      
                SecondaRiga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[2]"
                Check_SecondaRiga = driver.find_element_by_xpath(SecondaRiga)
                if Check_SecondaRiga.is_displayed():
                    print('I Tabella - II riga ESISTENTE SureBet 1X2')
                    with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_II_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "I Tabella - II riga ESISTENTE SureBet 1X2")
                        myfile.close()
                # Pulizia tutti i file  
                    I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_riga()
            # Inizio scrape I Tabella II riga

                # Settings
                    CampionatoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Campionato.html"
                    CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[2]/td[1]/div[1]/a[2]"

                    DataEventoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/DataEvento.html"
                    DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[5]/span[1]"

                    OraEventoFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/OraEvento.html"
                    OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[5]/span[3]"

                    PartitaFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Partita.html"
                    PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[2]"

                    PercentualeQuoteFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/PercentualeQuote.html"
                    PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[4]"

                    Quota1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Quota1.html"
                    Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[2]"

                    QuotaXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/QuotaX.html"
                    QuotaXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[4]"

                    Quota2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Quota2.html"
                    Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[6]"

                    # Modificare path anche nel file Bookmaker1.py
                    Bookmaker1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Bookmaker1.html"
                    Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[3]/div/a/img"

                    # Modificare path anche nel file BookmakerX.py
                    BookmakerXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/BookmakerX.html"
                    BookmakerXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[5]/div/a/img"

                    # Modificare path anche nel file Bookmaker2.py
                    Bookmaker2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/II_Riga/Bookmaker2.html"
                    Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[7]/div/a/img"
         
                #Campionato
                    I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                    with open(CampionatoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Campionato) 
                        myfile.close() 

                #DataEvento
                    I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                    with open(DataEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_DataEvento) 
                        myfile.close()

                #Oraevento
                    I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                    with open(OraEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_OraEvento) 
                        myfile.close()
                                                               
                #Partita
                    I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                    with open(PartitaFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Partita) 
                        myfile.close()
                    Partita_Cut = open(PartitaFile).read()
                    STRPartita = Partita_Cut[2:-2]
                    with open(PartitaFile, "w") as myfile:
                        myfile.write(STRPartita + '\n')
                        myfile.close()

                #PercentualeQuote
                    I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                    with open(PercentualeQuoteFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                    myfile.close()

                #Quota1
                    I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                    with open(Quota1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota1) 
                        myfile.close()
            
                #Bookmaker1
                    I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                    with open(Bookmaker1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                        myfile.close()
                    I_Tabella_II_Riga_Bookmaker1.Get_Bookmaker1();            
            
                #QuotaX
                    I_Tabella_QuotaX = driver.find_element_by_xpath(QuotaXXpath).text    
                    with open(QuotaXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_QuotaX) 
                        myfile.close()

                #BookmakerX
                    I_Tabella_BookmakerX = driver.find_element_by_xpath(BookmakerXXpath).get_attribute('alt')
                    with open(BookmakerXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_BookmakerX) 
                        myfile.close()
                    I_Tabella_II_Riga_BookmakerX.Get_BookmakerX()
                   
                #Quota2
                    I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                    with open(Quota2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota2) 
                        myfile.close()
            
                #Bookmaker2
                    I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                    with open(Bookmaker2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                        myfile.close()
                    I_Tabella_II_Riga_Bookmaker2.Get_Bookmaker2()
            
            except NoSuchElementException:
            # Non esiste I Tabella - II riga
                print('I Tabella - II riga NON esistente SureBet 1X2')
                with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_II_Riga.txt", "w") as myfile:
                    myfile.write("%s\n" % "I Tabella - II riga NON esistente SureBet 1X2")
                    myfile.close()
            # Pulizia tutti i file  
                I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_riga()

                # Inizio Scrape II Tabella 

    # Controllo se esiste I TABELLA - III RIGA       
            try:
                                      
                TerzaRiga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[2]"
                Check_TerzaRiga = driver.find_element_by_xpath(TerzaRiga)
                if Check_TerzaRiga.is_displayed():
                    print('I Tabella - III riga ESISTENTE SureBet 1X2')
                    with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_III_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "I Tabella - III riga ESISTENTE SureBet 1X2")
                        myfile.close()
                # Pulizia tutti i file  
                    I_Tabella_III_Riga_Clean.Clean_1x2_I_Tabella_III_riga()
            # Inizio scrape I Tabella II riga

                # Settings
                    CampionatoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Campionato.html"
                    CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[3]/td[1]/div[1]/a[2]"

                    DataEventoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/DataEvento.html"
                    DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[5]/span[1]"

                    OraEventoFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/OraEvento.html"
                    OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[5]/span[3]"

                    PartitaFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Partita.html"
                    PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[2]"

                    PercentualeQuoteFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/PercentualeQuote.html"
                    PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[4]"

                    Quota1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Quota1.html"
                    Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[2]"

                    QuotaXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/QuotaX.html"
                    QuotaXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[4]"

                    Quota2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Quota2.html"
                    Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[6]"

                    # Modificare path anche nel file Bookmaker1.py
                    Bookmaker1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Bookmaker1.html"
                    Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[3]/div/a/img"

                    # Modificare path anche nel file BookmakerX.py
                    BookmakerXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/BookmakerX.html"
                    BookmakerXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[5]/div/a/img"

                    # Modificare path anche nel file Bookmaker2.py
                    Bookmaker2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/III_Riga/Bookmaker2.html"
                    Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[7]/div/a/img"
         
                #Campionato
                    I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                    with open(CampionatoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Campionato) 
                        myfile.close() 

                #DataEvento
                    I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                    with open(DataEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_DataEvento) 
                        myfile.close()

                #Oraevento
                    I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                    with open(OraEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_OraEvento) 
                        myfile.close()
                                                               
                #Partita
                    I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                    with open(PartitaFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Partita) 
                        myfile.close()
                    Partita_Cut = open(PartitaFile).read()
                    STRPartita = Partita_Cut[2:-2]
                    with open(PartitaFile, "w") as myfile:
                        myfile.write(STRPartita + '\n')
                        myfile.close()

                #PercentualeQuote
                    I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                    with open(PercentualeQuoteFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                    myfile.close()

                #Quota1
                    I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                    with open(Quota1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota1) 
                        myfile.close()
            
                #Bookmaker1
                    I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                    with open(Bookmaker1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker1.Get_Bookmaker1();            
            
                #QuotaX
                    I_Tabella_QuotaX = driver.find_element_by_xpath(QuotaXXpath).text    
                    with open(QuotaXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_QuotaX) 
                        myfile.close()

                #BookmakerX
                    I_Tabella_BookmakerX = driver.find_element_by_xpath(BookmakerXXpath).get_attribute('alt')
                    with open(BookmakerXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_BookmakerX) 
                        myfile.close()
                    I_Tabella_III_Riga_BookmakerX.Get_BookmakerX()
                   
                #Quota2
                    I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                    with open(Quota2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota2) 
                        myfile.close()
            
                #Bookmaker2
                    I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                    with open(Bookmaker2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker2.Get_Bookmaker2()
            
            except NoSuchElementException:
            # Non esiste I Tabella - II riga
                print('I Tabella - III riga NON esistente SureBet 1X2')
                with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_III_Riga.txt", "w") as myfile:
                    myfile.write("%s\n" % "I Tabella - III riga NON esistente SureBet 1X2")
                    myfile.close()
            # Pulizia tutti i file  
                I_Tabella_III_Riga_Clean.Clean_1x2_I_Tabella_III_riga()

    # Controllo se esiste I TABELLA - IV RIGA       
            try:
                                      
                QuartRiga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[2]"
                Check_QuartRiga = driver.find_element_by_xpath(QuartRiga)
                if Check_QuartRiga.is_displayed():
                    print('I Tabella - IV riga ESISTENTE SureBet 1X2')
                    with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_IV_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "I Tabella - IV riga ESISTENTE SureBet 1X2")
                        myfile.close()
                # Pulizia tutti i file  
                    I_Tabella_IV_Riga_Clean.Clean_1x2_I_Tabella_IV_Riga()
            # Inizio scrape I Tabella IV riga

                # Settings
                    CampionatoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Campionato.html"
                    CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[4]/td[1]/div[1]/a[2]"

                    DataEventoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/DataEvento.html"
                    DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[5]/span[1]"

                    OraEventoFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/OraEvento.html"
                    OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[5]/span[3]"

                    PartitaFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Partita.html"
                    PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[2]"

                    PercentualeQuoteFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/PercentualeQuote.html"
                    PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[4]"

                    Quota1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Quota1.html"
                    Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[2]"

                    QuotaXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/QuotaX.html"
                    QuotaXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[4]"

                    Quota2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Quota2.html"
                    Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[6]"

                    # Modificare path anche nel file Bookmaker1.py
                    Bookmaker1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Bookmaker1.html"
                    Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[3]/div/a/img"

                    # Modificare path anche nel file BookmakerX.py
                    BookmakerXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/BookmakerX.html"
                    BookmakerXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[5]/div/a/img"

                    # Modificare path anche nel file Bookmaker2.py
                    Bookmaker2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/IV_Riga/Bookmaker2.html"
                    Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[7]/div/a/img"
         
                #Campionato
                    I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                    with open(CampionatoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Campionato) 
                        myfile.close() 

                #DataEvento
                    I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                    with open(DataEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_DataEvento) 
                        myfile.close()

                #Oraevento
                    I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                    with open(OraEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_OraEvento) 
                        myfile.close()
                                                               
                #Partita
                    I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                    with open(PartitaFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Partita) 
                        myfile.close()
                    Partita_Cut = open(PartitaFile).read()
                    STRPartita = Partita_Cut[2:-2]
                    with open(PartitaFile, "w") as myfile:
                        myfile.write(STRPartita + '\n')
                        myfile.close()

                #PercentualeQuote
                    I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                    with open(PercentualeQuoteFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                    myfile.close()

                #Quota1
                    I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                    with open(Quota1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota1) 
                        myfile.close()
            
                #Bookmaker1
                    I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                    with open(Bookmaker1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker1.Get_Bookmaker1();            
            
                #QuotaX
                    I_Tabella_QuotaX = driver.find_element_by_xpath(QuotaXXpath).text    
                    with open(QuotaXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_QuotaX) 
                        myfile.close()

                #BookmakerX
                    I_Tabella_BookmakerX = driver.find_element_by_xpath(BookmakerXXpath).get_attribute('alt')
                    with open(BookmakerXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_BookmakerX) 
                        myfile.close()
                    I_Tabella_III_Riga_BookmakerX.Get_BookmakerX()
                   
                #Quota2
                    I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                    with open(Quota2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota2) 
                        myfile.close()
            
                #Bookmaker2
                    I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                    with open(Bookmaker2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker2.Get_Bookmaker2()
            
            except NoSuchElementException:
            # Non esiste I Tabella - II riga
                print('I Tabella - IV riga NON esistente SureBet 1X2')
                with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_IV_Riga.txt", "w") as myfile:
                    myfile.write("%s\n" % "I Tabella - IV riga NON esistente SureBet 1X2")
                    myfile.close()
            # Pulizia tutti i file  
                I_Tabella_IV_Riga_Clean.Clean_1x2_I_Tabella_IV_Riga()

    # Controllo se esiste I TABELLA - V RIGA       
            try:
                                      
                QuintaRiga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[2]"
                Check_QuintaRiga = driver.find_element_by_xpath(QuintaRiga)
                if Check_QuintaRiga.is_displayed():
                    print('I Tabella - V riga ESISTENTE SureBet 1X2')
                    with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_V_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "I Tabella - V riga ESISTENTE SureBet 1X2")
                        myfile.close()
                # Pulizia tutti i file  
                    I_Tabella_V_Riga_Clean.Clean_1x2_I_Tabella_V_Riga()
            # Inizio scrape I Tabella IV riga

                # Settings
                    CampionatoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Campionato.html"
                    CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[5]/td[1]/div[1]/a[2]"

                    DataEventoFile = "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/DataEvento.html"
                    DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[5]/span[1]"

                    OraEventoFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/OraEvento.html"
                    OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[5]/span[3]"

                    PartitaFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Partita.html"
                    PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[2]"

                    PercentualeQuoteFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/PercentualeQuote.html"
                    PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[4]"

                    Quota1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Quota1.html"
                    Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[2]"

                    QuotaXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/QuotaX.html"
                    QuotaXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[4]"

                    Quota2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Quota2.html"
                    Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[6]"

                    # Modificare path anche nel file Bookmaker1.py
                    Bookmaker1File =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Bookmaker1.html"
                    Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[3]/div/a/img"

                    # Modificare path anche nel file BookmakerX.py
                    BookmakerXFile =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/BookmakerX.html"
                    BookmakerXXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[5]/div/a/img"

                    # Modificare path anche nel file Bookmaker2.py
                    Bookmaker2File =  "SureBet_HTML/SureBet_1X2/I_Tabella/V_Riga/Bookmaker2.html"
                    Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[7]/div/a/img"
         
                #Campionato
                    I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                    with open(CampionatoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Campionato) 
                        myfile.close() 

                #DataEvento
                    I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                    with open(DataEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_DataEvento) 
                        myfile.close()

                #Oraevento
                    I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                    with open(OraEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_OraEvento) 
                        myfile.close()
                                                               
                #Partita
                    I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                    with open(PartitaFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Partita) 
                        myfile.close()
                    Partita_Cut = open(PartitaFile).read()
                    STRPartita = Partita_Cut[2:-2]
                    with open(PartitaFile, "w") as myfile:
                        myfile.write(STRPartita + '\n')
                        myfile.close()

                #PercentualeQuote
                    I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                    with open(PercentualeQuoteFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                    myfile.close()

                #Quota1
                    I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                    with open(Quota1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota1) 
                        myfile.close()
            
                #Bookmaker1
                    I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                    with open(Bookmaker1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker1.Get_Bookmaker1();            
            
                #QuotaX
                    I_Tabella_QuotaX = driver.find_element_by_xpath(QuotaXXpath).text    
                    with open(QuotaXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_QuotaX) 
                        myfile.close()

                #BookmakerX
                    I_Tabella_BookmakerX = driver.find_element_by_xpath(BookmakerXXpath).get_attribute('alt')
                    with open(BookmakerXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_BookmakerX) 
                        myfile.close()
                    I_Tabella_III_Riga_BookmakerX.Get_BookmakerX()
                   
                #Quota2
                    I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                    with open(Quota2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota2) 
                        myfile.close()
            
                #Bookmaker2
                    I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                    with open(Bookmaker2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                        myfile.close()
                    I_Tabella_III_Riga_Bookmaker2.Get_Bookmaker2()
            
            except NoSuchElementException:
            # Non esiste I Tabella - II riga
                print('I Tabella - IV riga NON esistente SureBet 1X2')
                with open("Messaggi/SureBet_1X2/I_Tabella/Msg_I_Tabella_V_Riga.txt", "w") as myfile:
                    myfile.write("%s\n" % "I Tabella - V riga NON esistente SureBet 1X2")
                    myfile.close()
            # Pulizia tutti i file  
                I_Tabella_V_Riga_Clean.Clean_1x2_I_Tabella_V_Riga()

       
       # INIZIO SCRAPING II TABELLA - I RIGA       
            try:
                                      
                II_Tabella_I_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[2]"
                Check_II_Tabella_I_Riga = driver.find_element_by_xpath(II_Tabella_I_Riga)
                if Check_II_Tabella_I_Riga.is_displayed():
                    print('II Tabella - I riga ESISTENTE SureBet 1X2')
                    with open("Messaggi/SureBet_1X2/II_Tabella/Msg_II_Tabella_I_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "II Tabella - I riga ESISTENTE SureBet 1X2")
                        myfile.close()
                # Pulizia tutti i file  
                    # I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_riga()
            # Inizio scrape I Tabella II riga

                # Settings
                    CampionatoFile = "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Campionato.html"
                    CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[2]/td[1]/div[1]/a[2]"

                    DataEventoFile = "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/DataEvento.html"
                    DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[5]/span[1]"

                    OraEventoFile =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/OraEvento.html"
                    OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[5]/span[3]"

                    PartitaFile =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Partita.html"
                    PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[2]"

                    PercentualeQuoteFile =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/PercentualeQuote.html"
                    PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[4]"

                    Quota1File =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Quota1.html"
                    Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[2]"

                    QuotaXFile =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/QuotaX.html"
                    QuotaXXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[4]"

                    Quota2File =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Quota2.html"
                    Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[6]"

                    # Modificare path anche nel file Bookmaker1.py
                    Bookmaker1File =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Bookmaker1.html"
                    Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[3]/div/a/img"

                    # Modificare path anche nel file BookmakerX.py
                    BookmakerXFile =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/BookmakerX.html"
                    BookmakerXXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[5]/div/a/img"

                    # Modificare path anche nel file Bookmaker2.py
                    Bookmaker2File =  "SureBet_HTML/SureBet_1X2/II_Tabella/I_Riga/Bookmaker2.html"
                    Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[7]/div/a/img"
         
                #Campionato
                    I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                    with open(CampionatoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Campionato) 
                        myfile.close() 

                #DataEvento
                    I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                    with open(DataEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_DataEvento) 
                        myfile.close()

                #Oraevento
                    I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                    with open(OraEventoFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_OraEvento) 
                        myfile.close()
                                                               
                #Partita
                    I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                    with open(PartitaFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Partita) 
                        myfile.close()
                    Partita_Cut = open(PartitaFile).read()
                    STRPartita = Partita_Cut[2:-2]
                    with open(PartitaFile, "w") as myfile:
                        myfile.write(STRPartita + '\n')
                        myfile.close()

                #PercentualeQuote
                    I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                    with open(PercentualeQuoteFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                    myfile.close()

                #Quota1
                    I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                    with open(Quota1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota1) 
                        myfile.close()
            
                #Bookmaker1
                    I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                    with open(Bookmaker1File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                        myfile.close()
                    II_Tabella_I_Riga_Bookmaker1.Get_Bookmaker1();            
            
                #QuotaX
                    I_Tabella_QuotaX = driver.find_element_by_xpath(QuotaXXpath).text    
                    with open(QuotaXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_QuotaX) 
                        myfile.close()

                #BookmakerX
                    I_Tabella_BookmakerX = driver.find_element_by_xpath(BookmakerXXpath).get_attribute('alt')
                    with open(BookmakerXFile, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_BookmakerX) 
                        myfile.close()
                    II_Tabella_I_Riga_BookmakerX.Get_BookmakerX()
                   
                #Quota2
                    I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                    with open(Quota2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Quota2) 
                        myfile.close()
            
                #Bookmaker2
                    I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                    with open(Bookmaker2File, "w") as myfile:
                        myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                        myfile.close()
                    II_Tabella_I_Riga_Bookmaker2.Get_Bookmaker2()
            
            except NoSuchElementException:
            # Non esiste I Tabella - II riga
                print('II Tabella - I riga NON esistente SureBet 1X2')
                with open("Messaggi/SureBet_1X2/II_Tabella/Msg_II_Tabella_I_Riga.txt", "w") as myfile:
                    myfile.write("%s\n" % "II Tabella - I riga NON esistente SureBet 1X2")
                    myfile.close()
            # Pulizia tutti i file  
                # I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_riga()

                # Inizio Scrape II Tabella 
