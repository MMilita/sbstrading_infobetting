    # INIZIO SCRAPING I TABELLA - II RIGA
                    try:                            
                        I_Tabella_II_Riga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[2]/td[1]/div[4]"
                        Check_I_Tabella_II_Riga = driver.find_element_by_xpath(I_Tabella_II_Riga)
                        if Check_I_Tabella_II_Riga.is_displayed():
                            print('I Tabella - II Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_II_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "I Tabella - II Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_Riga()

                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/I_Tabella/II_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/I_Tabella/II_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/I_Tabella/II_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[6]/div/a/img"

                        #Campionato
                            I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_I_Tabella_II_Riga_Bookmaker1.Get_Bookmaker1();   

                        #Quota2
                            I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_I_Tabella_II_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                            # Non esiste I Tabella - II riga
                            print('I Tabella - II Riga NON ESISTENTE Tennis')
                            with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_II_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "I Tabella - II Riga NON ESISTENTE Tennis")
                                myfile.close()
                            # Pulizia tutti i file  
                            Tennis_I_Tabella_II_Riga_Clean.Clean_1x2_I_Tabella_II_Riga() 

    # INIZIO SCRAPING I TABELLA - III Riga
                    try:              
                                                
                        I_Tabella_III_Riga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[3]/td[1]/div[4]"
                        Check_I_Tabella_III_Riga = driver.find_element_by_xpath(I_Tabella_III_Riga)
                        if Check_I_Tabella_III_Riga.is_displayed():
                            print('I Tabella - III Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_III_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "I Tabella - III Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_I_Tabella_III_Riga_Clean.Clean_1x2_I_Tabella_III_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/I_Tabella/III_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/I_Tabella/III_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/I_Tabella/III_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[6]/div/a/img"

                        #Campionato
                            I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_I_Tabella_III_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_I_Tabella_III_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste I Tabella - III Riga
                        print('I Tabella - III Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_III_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "I Tabella - III Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_I_Tabella_III_Riga_Clean.Clean_1x2_I_Tabella_III_Riga() 

    # INIZIO SCRAPING I TABELLA - IV Riga
                    try:                                      
                        I_Tabella_IV_Riga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[4]/td[1]/div[4]"
                        Check_I_Tabella_IV_Riga = driver.find_element_by_xpath(I_Tabella_IV_Riga)
                        if Check_I_Tabella_IV_Riga.is_displayed():
                            print('I Tabella - IV Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_IV_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "I Tabella - IV Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_I_Tabella_IV_Riga_Clean.Clean_1x2_I_Tabella_IV_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/I_Tabella/IV_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/I_Tabella/IV_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[6]/div/a/img"

                        #Campionato
                            I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_I_Tabella_IV_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_I_Tabella_IV_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste I Tabella - IV Riga
                        print('I Tabella - IV Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_IV_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "I Tabella - IV Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_I_Tabella_IV_Riga_Clean.Clean_1x2_I_Tabella_IV_Riga() 

    # INIZIO SCRAPING I TABELLA - V Riga
                    try:                                      
                        I_Tabella_V_Riga = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[5]/td[1]/div[4]"
                        Check_I_Tabella_V_Riga = driver.find_element_by_xpath(I_Tabella_V_Riga)
                        if Check_I_Tabella_V_Riga.is_displayed():
                            print('I Tabella - V Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_V_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "I Tabella - V Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_I_Tabella_V_Riga_Clean.Clean_1x2_I_Tabella_V_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/I_Tabella/V_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[1]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/I_Tabella/V_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/I_Tabella/V_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[1]/tbody/tr[6]/td[6]/div/a/img"

                        #Campionato
                            I_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            I_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            I_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            I_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            I_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            I_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            I_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_I_Tabella_V_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            I_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            I_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % I_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_I_Tabella_V_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste I Tabella - V Riga
                        print('I Tabella - V Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/I_Tabella/Msg_Tennis_I_Tabella_V_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "I Tabella - V Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_I_Tabella_V_Riga_Clean.Clean_1x2_I_Tabella_V_Riga() 

    # INIZIO SCRAPING II Tabella - I Riga
                    try:               
                                       
                        II_Tabella_I_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[1]/div[4]"
                        Check_II_Tabella_I_Riga = driver.find_element_by_xpath(II_Tabella_I_Riga)
                        if Check_II_Tabella_I_Riga.is_displayed():
                            print('II Tabella - I Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_I_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "II Tabella - I Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_II_Tabella_I_Riga_Clean.Clean_1x2_II_Tabella_I_riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/II_Tabella/I_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/II_Tabella/I_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/II_Tabella/I_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[1]/td[6]/div/a/img"

                        #Campionato
                            II_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            II_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            II_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            II_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            II_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            II_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            II_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_II_Tabella_I_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            II_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            II_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_II_Tabella_I_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste II Tabella - I Riga
                        print('II Tabella - I Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_I_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "II Tabella - I Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_II_Tabella_I_Riga_Clean.Clean_1x2_II_Tabella_I_riga() 

    # INIZIO SCRAPING II Tabella - II Riga
                    try:                                      
                        II_Tabella_II_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[4]"
                        Check_II_Tabella_II_Riga = driver.find_element_by_xpath(II_Tabella_II_Riga)
                        if Check_II_Tabella_II_Riga.is_displayed():
                            print('II Tabella - II Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_II_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "II Tabella - II Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_II_Tabella_II_Riga_Clean.Clean_1x2_II_Tabella_II_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/II_Tabella/II_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/II_Tabella/II_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/II_Tabella/II_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[2]/td[6]/div/a/img"

                        #Campionato
                            II_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            II_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            II_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            II_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            II_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            II_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            II_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_II_Tabella_II_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            II_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            II_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_II_Tabella_II_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste II Tabella - II Riga
                        print('II Tabella - II Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_II_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "II Tabella - II Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_II_Tabella_II_Riga_Clean.Clean_1x2_II_Tabella_II_Riga() 

    # INIZIO SCRAPING II Tabella - III Riga
                    try:                                      
                        II_Tabella_III_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[1]/div[4]"
                        Check_II_Tabella_III_Riga = driver.find_element_by_xpath(II_Tabella_III_Riga)
                        if Check_II_Tabella_III_Riga.is_displayed():
                            print('II Tabella - III Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_III_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "II Tabella - III Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_II_Tabella_III_Riga_Clean.Clean_1x2_II_Tabella_III_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/II_Tabella/III_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/II_Tabella/III_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/II_Tabella/III_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[3]/td[6]/div/a/img"

                        #Campionato
                            II_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            II_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            II_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            II_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            II_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            II_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            II_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_II_Tabella_III_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            II_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            II_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_II_Tabella_III_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste II Tabella - III Riga
                        print('II Tabella - III Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_III_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "II Tabella - III Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_II_Tabella_III_Riga_Clean.Clean_1x2_II_Tabella_III_Riga() 

    # INIZIO SCRAPING II Tabella - IV Riga
                    try:                                      
                        II_Tabella_IV_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[1]/div[4]"
                        Check_II_Tabella_IV_Riga = driver.find_element_by_xpath(II_Tabella_IV_Riga)
                        if Check_II_Tabella_IV_Riga.is_displayed():
                            print('II Tabella - IV Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_IV_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "II Tabella - IV Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_II_Tabella_IV_Riga_Clean.Clean_1x2_II_Tabella_IV_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/II_Tabella/IV_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/II_Tabella/IV_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[4]/td[6]/div/a/img"

                        #Campionato
                            II_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            II_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            II_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            II_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            II_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            II_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            II_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_II_Tabella_IV_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            II_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            II_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_II_Tabella_IV_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste II Tabella - IV Riga
                        print('II Tabella - IV Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_IV_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "II Tabella - IV Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_II_Tabella_IV_Riga_Clean.Clean_1x2_II_Tabella_IV_Riga() 

    # INIZIO SCRAPING II Tabella - V Riga
                    try:                                      
                        II_Tabella_V_Riga = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[1]/div[4]"
                        Check_III_Tabella_V_Riga = driver.find_element_by_xpath(II_Tabella_V_Riga)
                        if Check_II_Tabella_V_Riga.is_displayed():
                            print('II Tabella - V Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_V_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "II Tabella - V Riga ESISTENTE Tennis")
                                myfile.close()
                        # Pulizia tutti i file  
                            Tennis_II_Tabella_V_Riga_Clean.Clean_1x2_II_Tabella_V_Riga()


                        # Settings
                            CampionatoFile = "SureBet_HTML/Tennis/II_Tabella/V_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[2]/thead/tr[1]/td[1]/div[1]/a[2]"

                            DataEventoFile = "SureBet_HTML/Tennis/II_Tabella/V_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[1]/div[4]/span[1]"


                            OraEventoFile =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[1]/div[4]/span[3]"


                            PartitaFile =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[1]/div[2]"

                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[2]/div"

                            Quota1File =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[3]"

                            Quota2File =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[6]"

                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[4]/div/a/img"

                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/II_Tabella/V_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[2]/tbody/tr[5]/td[6]/div/a/img"

                        #Campionato
                            II_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Campionato) 
                                myfile.close() 

                        #DataEvento
                            II_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_DataEvento) 
                                myfile.close()

                        #Oraevento
                            II_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_OraEvento) 
                                myfile.close()

                        #Partita
                            II_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()

                        #PercentualeQuote
                            II_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_PercentualeQuote) 
                                myfile.close()

                        #Quota1
                            II_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota1) 
                                myfile.close()

                        #Bookmaker1
                            II_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_II_Tabella_V_Riga_Bookmaker1.Get_Bookmaker1();            


                        #Quota2
                            II_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Quota2) 
                                myfile.close()

                        #Bookmaker2
                            II_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % II_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_II_Tabella_V_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException:
                        # Non esiste II Tabella - V Riga
                        print('II Tabella - V Riga NON ESISTENTE Tennis')
                        with open("Messaggi/Tennis/II_Tabella/Msg_Tennis_II_Tabella_V_Riga.txt", "w") as myfile:
                            myfile.write("%s\n" % "II Tabella - V Riga NON ESISTENTE Tennis")
                            myfile.close()
                        # Pulizia tutti i file  
                        Tennis_II_Tabella_V_Riga_Clean.Clean_1x2_II_Tabella_V_Riga() 
