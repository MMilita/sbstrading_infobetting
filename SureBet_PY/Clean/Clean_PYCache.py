import shutil

def Pulizia_PYCache():
    shutil.rmtree('SureBet_PY/Log/__pycache__')
    shutil.rmtree('SureBet_PY/Clean/__pycache__')
    shutil.rmtree('SureBet_PY/SureBet_1X2/__pycache__')
    #Clean cache I tabella
    shutil.rmtree('SureBet_PY/SureBet_1X2/I_Tabella/I_Riga/__pycache__')
    
    
