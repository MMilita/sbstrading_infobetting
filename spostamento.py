    #INIIZIO SCRAPING VI Tabella - V Riga
    
                # Pulizia tutti i file  
                    Tennis_IV_Tabella_V_Riga_Clean.Clean_1x2_IV_Tabella_V_Riga()
                    with open("Messaggi/Tennis/IV_Tabella/Msg_Tennis_IV_Tabella_V_Riga.txt", "w") as myfile:
                        myfile.write("%s\n" % "VI Tabella - V Riga NON ESISTENTE Tennis")
                        myfile.close()

                    try:
                        
                        Check_IV_Tabella_V_Riga = driver.find_element_by_xpath(r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[1]/div[2]")
                        if Check_IV_Tabella_V_Riga.is_displayed():
                            print('VI Tabella - V Riga ESISTENTE Tennis')
                            with open("Messaggi/Tennis/IV_Tabella/Msg_Tennis_IV_Tabella_V_Riga.txt", "w") as myfile:
                                myfile.write("%s\n" % "VI Tabella - V Riga ESISTENTE Tennis")
                                myfile.close()

                            # Pulizia tutti i file  
                            Tennis_IV_Tabella_V_Riga_Clean.Clean_1x2_IV_Tabella_V_Riga()

                        # Settings
                        
                            CampionatoFile = "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Campionato.html"
                            CampionatoXpath = r"//*[@id='ib_body_div']/div/table[5]/thead/tr[1]/td[1]/div[1]/a[2]"
                            
                            DataEventoFile = "SureBet_HTML/Tennis/IV_Tabella/V_Riga/DataEvento.html"
                            DataEventoXpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[1]/div[5]/span[1]"
                            OraEventoFile =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/OraEvento.html"
                            OraEventoXpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[1]/div[5]/span[3]"
                            PartitaFile =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Partita.html"
                            PartitaXPath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[1]/div[2]"
                            
                            PercentualeQuoteFile =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/PercentualeQuote.html"
                            PercentualeQuoteXpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[1]/div[4]"
                            
                            Quota1File =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Quota1.html"
                            Quota1Xpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[2]"
                            
                            Quota2File =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Quota2.html"
                            Quota2Xpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[4]"
                            
                            # Modificare path anche nel file Bookmaker1.py
                            Bookmaker1File =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Bookmaker1.html"
                            Bookmaker1Xpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[3]/div/a/img"
                            
                            # Modificare path anche nel file Bookmaker2.py
                            Bookmaker2File =  "SureBet_HTML/Tennis/IV_Tabella/V_Riga/Bookmaker2.html"
                            Bookmaker2Xpath = r"//*[@id='ib_body_div']/div/table[5]/tbody/tr[5]/td[5]/div/a/img"
                        #Campionato
                            IV_Tabella_Campionato = driver.find_element_by_xpath(CampionatoXpath).text
                            with open(CampionatoFile, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Campionato) 
                                myfile.close() 
                        #DataEvento
                            IV_Tabella_DataEvento = driver.find_element_by_xpath(DataEventoXpath).text
                            with open(DataEventoFile, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_DataEvento) 
                                myfile.close()
                        #Oraevento
                            IV_Tabella_OraEvento = driver.find_element_by_xpath(OraEventoXpath).text
                            with open(OraEventoFile, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_OraEvento) 
                                myfile.close()
                        #Partita
                            IV_Tabella_Partita = driver.find_element_by_xpath(PartitaXPath).text.encode()
                            with open(PartitaFile, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Partita) 
                                myfile.close()
                            Partita_Cut = open(PartitaFile).read()
                            STRPartita = Partita_Cut[2:-2]
                            with open(PartitaFile, "w") as myfile:
                                myfile.write(STRPartita + '\n')
                                myfile.close()
                        #PercentualeQuote
                            IV_Tabella_PercentualeQuote = driver.find_element_by_xpath(PercentualeQuoteXpath).text
                            with open(PercentualeQuoteFile, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_PercentualeQuote) 
                                myfile.close()
                        #Quota1
                            IV_Tabella_Quota1 = driver.find_element_by_xpath(Quota1Xpath).text    
                            with open(Quota1File, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Quota1) 
                                myfile.close()
                        #Bookmaker1
                            IV_Tabella_Bookmaker1 = driver.find_element_by_xpath(Bookmaker1Xpath).get_attribute('alt')
                            with open(Bookmaker1File, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Bookmaker1) 
                                myfile.close()
                            Tennis_IV_Tabella_V_Riga_Bookmaker1.Get_Bookmaker1();   
                        #Quota2
                            IV_Tabella_Quota2 = driver.find_element_by_xpath(Quota2Xpath).text    
                            with open(Quota2File, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Quota2) 
                                myfile.close()
                        #Bookmaker2
                            IV_Tabella_Bookmaker2 = driver.find_element_by_xpath(Bookmaker2Xpath).get_attribute('alt')
                            with open(Bookmaker2File, "w") as myfile:
                                myfile.write("%s\n" % IV_Tabella_Bookmaker2) 
                                myfile.close()
                            Tennis_IV_Tabella_V_Riga_Bookmaker2.Get_Bookmaker2()

                    except NoSuchElementException: 

                        print('VI Tabella - V Riga NON ESISTENTE')

  